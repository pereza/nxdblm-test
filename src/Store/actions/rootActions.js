const HOST = "http://nxdblm-api.web.cern.ch/";

export function setFillByName(name) {
  return function(dispatch, getState) {
    if (getState().fill.name === name) {
    } else {
      return dispatch(getFillByName(name)).then(() => {
        if (getState().fill.id) {
          Promise.all([
            dispatch(getModesByFillId(getState().fill.id)),
            dispatch(getDevicesByFillId(getState().fill.id))
          ]).finally(() => {
            if (getState().fill.id && getState().modes && getState().devices) {
              dispatch(
                getMeasuresDatasForFillID(
                  getState().fill.id,
                  getState().modes,
                  getState().devices
                )
              );
            }
          });
        } else {
          dispatch(getModesByFillName(name));
          dispatch(getDevicesByFillName(name));
        }
      });
    }
  };
}

export function cleanStoreForName(name) {
  return dispatch => {
    dispatch({
      type: "CLEAN_STORE",
      name: name
    });
  };
}

/***
 * @return {function(*): Promise<any | void>}
 */
export function getFills() {
  return dispatch => {
    return fetch(HOST + "fills")
      .then(response => response.json())
      .then(response => {
        dispatch(displayFills(response.metadata));
      })
      .catch(err => console.error(err));
  };
}

function displayFills(data) {
  return {
    type: "SET_FILLS",
    data: data
  };
}

export function getFillById(id) {
  return (dispatch, getState) => {
    return fetch(HOST + "fill/metadata/id?id=" + id)
      .then(response => response.json())
      .then(response => {
        dispatch(SetFillMetaData(response.metadata));
      })
      .catch(err => console.error(err));
  };
}

export function getFillByName(name) {
  return (dispatch, getState) => {
    return fetch(HOST + "fill/metadata/name?name=" + name)
      .then(response => response.json())
      .then(response => {
        dispatch(SetFillMetaData(response.metadata));
      })
      .catch(err => console.error(err));
  };
}

function SetFillMetaData(data) {
  return {
    type: "SET_FILL_METADATA",
    data: data
  };
}

// Modes
export function getModesByFillId(id) {
  return dispatch => {
    return fetch(HOST + "fill/modes/id?id=" + id)
      .then(response => response.json())
      .then(response => {
        dispatch(setFillModes(response.modes));
      })
      .catch(err => console.error(err));
  };
}

export function getModesByFillName(name) {
  return dispatch => {
    return fetch(HOST + "fill/modes/name?name=" + name)
      .then(response => response.json())
      .then(response => {
        dispatch(setFillModes(response.modes));
      })
      .catch(err => console.error(err));
  };
}

function setFillModes(data) {
  return {
    type: "SET_FILL_MODES",
    data: data
  };
}

//Devices
export function getDevicesByFillName(name) {
  return (dispatch, getState) => {
    return fetch(HOST + "fill/devices/name?name=" + name)
      .then(res => res.json())
      .then(res => {
        dispatch(setFillDevice(res.devices));
      });
  };
}

function getDevicesByFillId(id) {
  return (dispatch, getState) => {
    return fetch(HOST + "fill/devices/id?id=" + id)
      .then(res => res.json())
      .then(res => {
        dispatch(setFillDevice(res.devices));
      });
  };
}

function setFillDevice(data) {
  return {
    type: "SET_FILL_DEVICES",
    devices: data
  };
}

// Get the Datas of grah by id
export function getGraphsDatasById(id) {
  return dispatch => {
    return fetch(HOST + "fills/graphs/name?name=" + id)
      .then(response => response.json())
      .then(response => {
        dispatch(displayGraphsById(response.data));
      })
      .catch(err => console.error(err));
  };
}

function displayGraphsById(data) {
  return {
    type: "SET_FILL_GRAPH_DATA",
    data: data
  };
}

//Get measures' data for graphs
/***
 *
 * @param fill_id
 * @param modes array of mode_id to get data for the fill
 * @param devices array of device_id to get data for the fill
 * @return
 */
export function getMeasuresDatasForFillID(fill_id, modes, devices) {
  return function(dispatch) {
    modes.forEach(mode => {
      devices.forEach(device => {
        dispatch(
          getHistogramHGDataForModeID_DeviceID(fill_id, mode.id, device.id)
        );
      });
      devices.forEach(device => {
        dispatch(
          getHistogramHGIntimeDataForModeID_Device_ID(
            fill_id,
            mode.id,
            device.id
          )
        );
      });
      devices.forEach(device => {
        dispatch(
          getIntegralHGDataForModeID_DeviceID(fill_id, mode.id, device.id)
        );
      });
      devices.forEach(device => {
        dispatch(
          getIntegralHGIntimeDataForMode_ID_Device_ID(
            fill_id,
            mode.id,
            device.id
          )
        );
      });
      devices.forEach(device => {
        dispatch(
          getIntegralDistHGDataForMode_ID_Device_ID(fill_id, mode.id, device.id)
        );
      });
      devices.forEach(device => {
        dispatch(
          getRawDistHGDataForMode_ID_Device_ID(fill_id, mode.id, device.id)
        );
      });
      devices.forEach(device => {
        dispatch(
          getTurnlossHGDataForMode_ID_Device_ID(fill_id, mode.id, device.id)
        );
      });
      devices.forEach(device => {
        dispatch(
          getTurnlossHGIntimeDataForMode_ID_Device_ID(
            fill_id,
            mode.id,
            device.id
          )
        );
      });
    });
  };
}

//Histograms
/***
 *
 * @param fill_id
 * @param fill_mode_id
 * @param device_id
 * @returns {Function}
 */
function getHistogramHGDataForModeID_DeviceID(
  fill_id,
  fill_mode_id,
  device_id
) {
  return dispatch => {
    fetch(
      HOST +
        `histogram/hg?fill_id=${fill_id}&fill_mode_id=${fill_mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addHistogramHGToMode(res, fill_mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function getHistogramHGIntimeDataForModeID_Device_ID(
  fill_id,
  fill_mode_id,
  device_id
) {
  return dispatch => {
    fetch(
      HOST +
        `histogram/hg_intime?fill_id=${fill_id}&fill_mode_id=${fill_mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addHistogramHGIntimeToMode(res, fill_mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addHistogramHGToMode(data, mode_id, device_id) {
  return {
    type: "ADD_HISTOGRAM_HG_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

function addHistogramHGIntimeToMode(data, mode_id, device_id) {
  return {
    type: "ADD_HISTOGRAM_HG_INTIME_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

//Integrals
function getIntegralHGDataForModeID_DeviceID(fill_id, mode_id, device_id) {
  return dispatch => {
    fetch(
      HOST +
        `integral/hg?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addIntegralHGToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function getIntegralHGIntimeDataForMode_ID_Device_ID(
  fill_id,
  mode_id,
  device_id
) {
  return dispatch => {
    fetch(
      HOST +
        `integral/hg_intime?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addIntegralHGIntimeToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addIntegralHGIntimeToMode(data, mode_id, device_id) {
  return {
    type: "ADD_INTEGRAL_HG_INTIME_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

function addIntegralHGToMode(data, mode_id, device_id) {
  return {
    type: "ADD_INTEGRAL_HG_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

//Integral Dist
function getIntegralDistHGDataForMode_ID_Device_ID(
  fill_id,
  mode_id,
  device_id
) {
  return dispatch => {
    fetch(
      HOST +
        `integral_dist/hg?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addIntegralDistHGToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addIntegralDistHGToMode(data, mode_id, device_id) {
  return {
    type: "ADD_INTEGRAL_DIST_HG_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

//Raw dist
function getRawDistHGDataForMode_ID_Device_ID(fill_id, mode_id, device_id) {
  return dispatch => {
    fetch(
      HOST +
        `raw_dist/hg?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addRawDistHGToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addRawDistHGToMode(data, mode_id, device_id) {
  return {
    type: "ADD_RAW_DIST_HG_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

//TurnLoss
function getTurnlossHGDataForMode_ID_Device_ID(fill_id, mode_id, device_id) {
  return dispatch => {
    fetch(
      HOST +
        `turnloss/hg?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addTurnlossHGToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addTurnlossHGToMode(data, mode_id, device_id) {
  return {
    type: "ADD_TURNLOSS_HG_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}

function getTurnlossHGIntimeDataForMode_ID_Device_ID(
  fill_id,
  mode_id,
  device_id
) {
  return dispatch => {
    fetch(
      HOST +
        `turnloss/hg_intime?fill_id=${fill_id}&fill_mode_id=${mode_id}&device_id=${device_id}`
    )
      .then(res => res.json())
      .then(res => {
        dispatch(addTurnlossHGIntimeToMode(res, mode_id, device_id));
      })
      .catch(err => console.error(err));
  };
}

function addTurnlossHGIntimeToMode(data, mode_id, device_id) {
  return {
    type: "ADD_TURNLOSS_HG_INTIME_MODE",
    data: {
      ...data,
      device_id: device_id
    },
    mode_id: mode_id
  };
}
