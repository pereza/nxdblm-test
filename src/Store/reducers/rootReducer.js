import { checkDataIsPulled } from "../../helpers/dataHelpers";

const initState = {
  fillsList: [], //  List of all fills + some datas to display

  fill: [], //  fill to display
  modes: [], //  List of modes
  devices: [],
  graphData: [],

  histogram_hg: [],
  histogram_hg_intime: [],

  integral_hg: [],
  integral_hg_intime: [],

  raw_distrib_hg: [],

  integral_distrib_hg: [],

  turnloss_hg: [],
  turnloss_hg_intime: [],

  histogram_hg_pulled: false,
  histogram_hg_intime_pulled: false,

  integral_hg_pulled: false,
  integral_hg_intime_pulled: false,

  raw_distrib_hg_pulled: false,
  integral_distrib_hg_pulled: false,

  turnloss_hg_pulled: false,
  turnloss_hg_intime_pulled: false,
};

const rootReducer = (state = initState, action) => {
  switch (action.type) {
    case "SET_FILLS":
      return {
        ...state,
        fillsList: action.data
      };
    case "SET_FILL_METADATA":
      return {
        ...state,
        fill: action.data
      };
    case "SET_FILL_MODES":
      return {
        ...state,
        modes: action.data,
        histogram_hg: action.data.map(e => {
          return [];
        }),
        histogram_hg_intime: action.data.map(e => {
          return [];
        }),
        integral_hg: action.data.map(e => {
          return [];
        }),
        integral_hg_intime: action.data.map(e => {
          return [];
        }),

        raw_distrib_hg: action.data.map(e => {
          return [];
        }),
        integral_distrib_hg: action.data.map(e => {
          return [];
        }),
        turnloss_hg: action.data.map(e => {
          return [];
        }),
        turnloss_hg_intime: action.data.map(e => {
          return [];
        })
      };
    case "SET_FILL_DEVICES":
      return {
        ...state,
        devices: action.devices
      };
    case "SET_FILL_GRAPH_DATA":
      return {
        ...state,
        graphData: action.data
      };
    case "ADD_HISTOGRAM_HG_MODE":
      let mode_idx_hist_hg = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newHistohg = state.histogram_hg;
      newHistohg[mode_idx_hist_hg].push(action.data);
      return {
        ...state,
        histogram_hg: newHistohg,
        histogram_hg_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newHistohg]
        )
      };
    case "ADD_HISTOGRAM_HG_INTIME_MODE":
      let mode_idx_hist_hg_IT = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newHistohgIntime = state.histogram_hg_intime;
      newHistohgIntime[mode_idx_hist_hg_IT].push(action.data);
      return {
        ...state,
        histogram_hg_intime: newHistohgIntime,
        histogram_hg_intime_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newHistohgIntime]
        )
      };

    case "ADD_INTEGRAL_HG_MODE":
      let mode_idx_int_hg = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newIntegralhg = state.integral_hg;
      newIntegralhg[mode_idx_int_hg].push(action.data);
      return {
        ...state,
        integral_hg: newIntegralhg,
        integral_hg_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newIntegralhg]
        )
      };
    case "ADD_INTEGRAL_HG_INTIME_MODE":
      let mode_idx_int_hg_IT = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newIntegralhgIntime = state.integral_hg_intime;
      newIntegralhgIntime[mode_idx_int_hg_IT].push(action.data);
      return {
        ...state,
        integral_hg_intime: newIntegralhgIntime,
        integral_hg_intime_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newIntegralhgIntime]
        )
      };

    case "ADD_RAW_DIST_HG_MODE":
      let mode_index_raw_dist_hg = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newRawDisthg = state.raw_distrib_hg;
      newRawDisthg[mode_index_raw_dist_hg].push(action.data);
      return {
        ...state,
        raw_distrib_hg: newRawDisthg,
        raw_distrib_hg_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newRawDisthg]
        )
      };

    case "ADD_INTEGRAL_DIST_HG_MODE":
      let mode_index_integral_dist_hg = state.modes.findIndex(
        mode => mode.id === action.mode_id
      );
      let newIntegralDisthg = state.integral_distrib_hg;
      newIntegralDisthg[mode_index_integral_dist_hg].push(action.data);
      return {
        ...state,
        integral_distrib_hg: newIntegralDisthg,
        integral_distrib_hg_pulled: checkDataIsPulled(
          state.modes.length,
          state.devices.length,
          [newIntegralDisthg]
        )
      };

    case "ADD_TURNLOSS_HG_MODE":
      let mode_index_turnloss_hg = state.modes.findIndex(
          mode => mode.id === action.mode_id
      );
      let newTurnlosshg = state.turnloss_hg;
      newTurnlosshg[mode_index_turnloss_hg].push(action.data);
      return {
        ...state,
        turnloss_hg: newTurnlosshg,
        turnloss_hg_pulled: checkDataIsPulled(
            state.modes.length,
            state.devices.length,
            [newTurnlosshg]
        )
      };

    case "ADD_TURNLOSS_HG_INTIME_MODE":
      let mode_index_turnloss_hg_intime = state.modes.findIndex(
          mode => mode.id === action.mode_id
      );
      let newTurnlosshgIntime = state.turnloss_hg_intime;
      newTurnlosshgIntime[mode_index_turnloss_hg_intime].push(action.data);
      return {
        ...state,
        turnloss_hg_intime: newTurnlosshgIntime,
        turnloss_hg_intime_pulled: checkDataIsPulled(
            state.modes.length,
            state.devices.length,
            [newTurnlosshgIntime]
        )
      };

    case "CLEAN_STORE":
      return action.name === state.fill.name
        ? { ...state }
        : {
            ...state,
            fill: {},
            modes: [],
            graphData: [],

            histogram_hg: [],
            histogram_hg_intime: [],

            integral_hg: [],
            integral_hg_intime: [],

            turnloss_hg: [],
            turnloss_hg_intime: [],

            dataPulled: false
          };

    default:
      return {
        ...state
      };
  }
};

export default rootReducer;
