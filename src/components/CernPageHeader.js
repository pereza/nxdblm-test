import React, { Component } from "react";

class CernPageHeader extends Component {
  render() {
    return (
      <div className="cern-header-container">
        <h1>
          <a href="//home.cern" title="CERN">
            {" "}
            CERN <span> Accelerating Science</span>
          </a>
        </h1>
      </div>
    );
  }
}

export default CernPageHeader;
