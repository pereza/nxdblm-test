import { Row } from "antd";
import React from "react";
import Description from "./Description";

function Content() {
  return (
    <Row>
      <Description term="Fill">{this.props.id}</Description>
      <Description term="Effective Time">2017-10-10</Description>
      <Description term="Start Time">{this.props.StartTime}</Description>
      <Description term="End Time">{this.props.EndTime}</Description>
    </Row>
  );
}

export default Content;
