import { Col, Pagination, Row, Tabs } from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";
import Content from "./Content";
import Graph from "./Graph";
import TabPane from "./TabPane";

class PageHeader extends Component {
  onChange = pageNumber => {
    console.log("Page: ", pageNumber);
  };

  render() {
    return (
      <PageHeader
        title="This is the very very first version of the web interface. Expect strange things to happen."
        footer={
          <div>
            <div className="mypagination">
              <Pagination
                showQuickJumper
                defaultCurrent={this.state.fills[0]}
                pageSize={1}
                total={this.state.fills.length}
                onChange={this.onChange}
              />
            </div>
            <Tabs defaultActiveKey="1" type="card">
              <TabPane tab="SETUP" key="1" />
              <TabPane tab="INJPROT" key="2" />
              <TabPane tab="INJPHYS" key="3" />
              <TabPane tab="PRERAMP" key="4" />
              <TabPane tab="RAMP" key="5" />
              <TabPane tab="FLATTOP" key="6" />
              <TabPane tab="SQUEEZE" key="7" />
              <TabPane tab="ADJUST" key="8" />
              <TabPane tab="STABLE" key="9" />
              <TabPane tab="BEAMDUMP" key="10" />
            </Tabs>
            <Row>
              <Col span={8} className="myCol">
                <Graph />
              </Col>
              <Col span={8} className="myCol">
                <Graph />
              </Col>
            </Row>
          </div>
        }
      >
        <div className="wrap">
          <div className="content padding">
            <Content fill />
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default PageHeader;
