import { Button, Icon, Modal } from "antd";
import React, { Component } from "react";

class FillListHeader extends Component {
  state = {
    showHelpModal: false,
    wikiLink:
      "https://wikis.cern.ch/display/BEBI/Web+Page+for+Diamonds+Detector+Loss+Monitoring",
    cernLink: "https://cern.ch"
  };

  render() {
    return (
      <div>
        <div className={"fill-list-header-container"}>
          <h2 className={"fill-list-header-title"}>
            Diamonds detector monitoring
          </h2>
          <div className={"fill-list-header-help-container"}>
            <Button
              type="primary"
              icon="question"
              shape="circle"
              onClick={() => this.setState({ showHelpModal: true })}
            />
          </div>
        </div>

        <Modal
          centered
          visible={this.state.showHelpModal}
          onOk={() => this.setState({ showHelpModal: false })}
          onCancel={() => this.setState({ showHelpModal: false })}
          title={
            <div>
              <Icon
                style={{ fontSize: "20px", paddingRight: "10px" }}
                type="question-circle"
                theme="twoTone"
              />{" "}
              Help
            </div>
          }
          footer={[
            <Button
              key="submit"
              type="primary"
              onClick={() => this.setState({ showHelpModal: false })}
            >
              Ok
            </Button>
          ]}
        >
          <div className={"help-modal-content"}>
            <p>
              Consult fill's data by clicking on the one you want to see in the
              list.
            </p>
            <p>
              If you have any problem, question or sugestion please go on the{" "}
              <a
                className={"help-modal-wiki-link"}
                href={this.state.wikiLink}
                target={"_blank"}
                onClick={() => this.setState({ showHelpModal: false })}
              >
                Wiki
              </a>
              .
            </p>
          </div>
        </Modal>
      </div>
    );
  }
}

export default FillListHeader;
