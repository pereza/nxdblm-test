import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsBoost from "highcharts/modules/boost";
import HighchartsExportData from "highcharts/modules/export-data";
import HighchartsExporting from "highcharts/modules/exporting";
import React, { Component } from "react";
HighchartsBoost(Highcharts);
HighchartsExporting(Highcharts);
HighchartsExportData(Highcharts);

class HCRawDist extends Component {
  state = {
    dataToRender: [],
    graphdata: []
  };

  componentDidMount = () => {
    let displaydatas = [];
    this.props.datasToPlot.forEach((e, index) => {
      displaydatas.push({
        boostThreshold: 1,
        turboThreshold: 1,
        step: "left",
        name: this.props.devices.find(d => d.id === e.device_id).name,
        data: e.dataPoints
      });
    });

    this.setState({
      graphdata: displaydatas
    });
  };

  render() {
    const options = {
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      boost: {
        enabled: true,
        seriesThreshold: 1
      },
      chart: {
        zoomType: "x",
        resetZoomButton: {
          position: {
            align: "left", // by default
            x: 10
          }
        }
      },
      exporting: {
        buttons: {
          contextButton: {
            menuItems: [
              "printChart",
              "separator",
              "downloadPNG",
              "downloadJPEG",
              "downloadPDF",
              "downloadSVG",
              "separator",
              "downloadCSV",
              "downloadXLS"
            ]
          }
        }
      },
      yAxis: {
        type: "logarithmic",
        minorTickInterval: 0.1,
        title: {
          text: "Loss"
        }
      },
      series: this.state.graphdata
    };

    return (
      <div>
        <HighchartsReact highcharts={Highcharts} options={options} />
      </div>
    );
  }
}

export default HCRawDist;
