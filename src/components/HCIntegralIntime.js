import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsBoost from "highcharts/modules/boost";
import HighchartsExportData from "highcharts/modules/export-data";
import HighchartsExporting from "highcharts/modules/exporting";
import React, { Component } from "react";
import { sortArrays } from "../helpers/dataHelpers";
HighchartsBoost(Highcharts);
HighchartsExporting(Highcharts);
HighchartsExportData(Highcharts);

class HCIntegralIntime extends Component {
  state = {
    dataToRender: [],
    graphdata: []
  };

  componentDidMount = () => {
    let displaydatas = [];
    this.props.datasToPlot.forEach((e, index) => {
      let sorted = sortArrays([e.timestamps, e.dataPoints]);
      displaydatas.push({
        boostThreshold: 1,
        turboThreshold: 1,
        step: "left",
        name: this.props.devices.find(d => d.id === e.device_id).name,
        data: sorted[1].map((dp, index) => {
          return [new Date(sorted[0][index] / 1000000), dp];
        })
      });
    });

    this.setState({
      graphdata: displaydatas
    });
  };

  render() {
    const options = {
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      boost: {
        enabled: true,
        seriesThreshold: 1
      },
      exporting: {
        buttons: {
          contextButton: {
            menuItems: [
              "printChart",
              "separator",
              "downloadPNG",
              "downloadJPEG",
              "downloadPDF",
              "downloadSVG",
              "separator",
              "downloadCSV",
              "downloadXLS"
            ]
          }
        }
      },
      chart: {
        zoomType: "x",
        resetZoomButton: {
          position: {
            align: "left", // by default
            // verticalAlign: 'top', // by default
            x: 10
          }
        }
      },
      xAxis: {
        labels: {
          formatter: function() {
            return Highcharts.dateFormat("%H:%M:%S", this.value);
          }
        },
        tickInterval: 60 * 60 * 1000
      },
      yAxis: {
        type: "logarithmic",
        minorTickInterval: 0.1,
        title: {
          text: "Loss"
        }
      },
      series: this.state.graphdata
    };

    return (
      <div>
        <HighchartsReact highcharts={Highcharts} options={options} />
      </div>
    );
  }
}

export default HCIntegralIntime;
