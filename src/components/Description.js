import { Col } from "antd";
import React from "react";

function Description({ term, children, span = 12 }) {
  return (
    <Col span={span}>
      <div className="description">
        <div className="term">{term}</div>
        <div className="detail">{children}</div>
      </div>
    </Col>
  );
}
export default Description;
