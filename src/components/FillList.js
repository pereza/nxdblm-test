import { Button, Icon, Input, Table } from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";
import Highlighter from "react-highlight-words";

class FillList extends Component {
  state = {
    searchText: ""
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          fontSize: 20,
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const data = [];
    this.props.data.forEach(d => {
      data.push({
        key: d.id,
        ...d
      });
    });
    data.sort((a, b) => a.name - b.name);

    const columns = [
      {
        title: "Fill N°",
        dataIndex: "name",
        key: "name",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.name - b.name,
        ...this.getColumnSearchProps("name")
      },
      {
        title: "Start Time",
        dataIndex: "start",
        defaultSortOrder: "descend",
        sorter: (a, b) => new Date(a.start) - new Date(b.start)
      },
      {
        title: "End Time",
        dataIndex: "end",
        defaultSortOrder: "descend",
        sorter: (a, b) => new Date(a.end) - new Date(b.end)
      }
    ];

    return (
      <div className={"fill-list-container"}>
        <Table
          columns={columns}
          dataSource={data}
          onRow={record => {
            return {
              onClick: event => {
                this.props.history.push("/fill/" + record["name"]);
              }
            };
          }}
          pagination={{
            defaultPageSize: 13,
            pageSizeOptions: ["5", "10", "13", "15", "50", "100"],
            hideOnSinglePage: false,
            showSizeChanger: true
          }}
          rowClassName={"fill-list-row"}
        />
      </div>
    );
  }
}

export default FillList;
