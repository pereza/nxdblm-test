import { Col, PageHeader, Row, Tabs } from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";
import GraphContainer from "../Containers/GraphContainer";
import "../index.css";

class FillData extends Component {
  state = {
    selectedMode: "",
    selectedIndex: 0
  };

  handleChangeKey = e => {
    this.setState({ selectedMode: this.props.modes[e].mode, selectedIndex: e });
  };

  componentDidMount() {
    this.setState({ selectedMode: this.props.modes[0].mode });
  }

  render() {
    const { TabPane } = Tabs;

    const Description = ({ term, children, span = 8 }) => (
      <Col span={span}>
        <div className="description">
          <div className="term">{term}</div>
          <div className="detail">{children}</div>
        </div>
      </Col>
    );

    const fillDescriptionContent = (
      <Row>
        <Description term="Start Time">{this.props.fill.start}</Description>
        <Description term="End Time">{this.props.fill.end}</Description>
      </Row>
    );

    return (
      <div>
        <PageHeader
          onBack={() => this.props.history.push("/")}
          title={this.props.fill.name}
          footer={
            <Tabs defaultActiveKey="0" onChange={this.handleChangeKey}>
              {this.props.modes.map((e, index) => (
                <TabPane tab={e.name} key={index} />
              ))}
            </Tabs>
          }
        >
          <div className="wrap">
            <div className="content padding">{fillDescriptionContent}</div>
          </div>
        </PageHeader>
        <GraphContainer
          name={this.props.fill.name}
          selectedMode={this.state.selectedMode}
          selectedModeIndex={this.state.selectedIndex}
        />
      </div>
    );
  }
}

export default FillData;
