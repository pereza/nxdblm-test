import React, { Component } from "react";
import CanvasJSReact from "../canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Step extends Component {
  render() {
    var dt = [];
    this.props.hs.forEach(e => {
      dt.push({ y: e });
    });

    const options = {
      zoomEnabled: true,
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Stock Movement"
      },
      axisY: {
        title: "Stock In Hand"
      },
      height: 800,
      data: [
        {
          type: "stepArea",
          dataPoints: dt
        }
      ]
    };

    return (
      <div>
        <h1>React Step-Line Chart</h1>
        <CanvasJSChart
          options={options}
          /* onRef={ref => this.chart = ref} */
        />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}

export default Step;
