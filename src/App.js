import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import CernPageHeader from "./components/CernPageHeader";
import FillDataContainer from "./Containers/FillDataContainer";
import FillsListContainer from "./Containers/FillsListContainer";

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div className="App">
          <CernPageHeader />
          <Switch>
            <Route exact path="/" component={FillsListContainer} />
            <Route path="/fill/:name" component={FillDataContainer} />
          </Switch>
        </div>
      </HashRouter>
    );
  }
}

export default App;
