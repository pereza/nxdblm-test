import React, { Component } from "react";
import { connect } from "react-redux";
import FillList from "../components/FillList";
import FillListHeader from "../components/FillsListHeader";
import * as actionCreators from "../Store/actions/rootActions";

class FillListContainer extends Component {
  componentDidMount() {
    this.props.getFills();
  }

  render() {
    return (
      <div>
        <FillListHeader />
        <FillList history={this.props.history} data={this.props.fillsList} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  actionCreators
)(FillListContainer);
