import { Col, Row } from "antd";
import React, { Component } from "react";
import ReactLoading from "react-loading";
import { connect } from "react-redux";
import HCHistogram from "../components/HCHistogram";
import HCHistogramIntime from "../components/HCHistogramIntime";
import HCIntegral from "../components/HCIntegral";
import HCIntegralDist from "../components/HCIntegralDist";
import HCIntegralIntime from "../components/HCIntegralIntime";
import HCRawDist from "../components/HCRawDist";
import HCTurnloss from "../components/HCTurnloss";
import HCTurnlossIntime from "../components/HCTurnlossIntime";
import "../index.css";
import * as actionCreators from "../Store/actions/rootActions";

class GraphContainer extends Component {
  state = {
    histogram_hg_toPlot: []
  };

  render() {
    const Graphs = ({ children, loading }) => {
      return loading ? (
        <div className={"loading"}>
          <div className={"loading-img"}>
            <ReactLoading
              type={"spinningBubbles"}
              color={"blue"}
              height={"100%"}
              width={"100%"}
            />
          </div>
        </div>
      ) : (
        <div className="Graphs">
          <div className="detail">{children}</div>
        </div>
      );
    };

    //go only when all the data is arrived on store
    const go =
      this.props.histogram_hg_pulled &&
      this.props.integral_distrib_hg_pulled &&
      this.props.histogram_hg_intime_pulled &&
      this.props.integral_hg_pulled &&
      this.props.integral_hg_intime_pulled &&
      this.props.raw_distrib_hg_pulled &&
      this.props.turnloss_hg_pulled &&
      this.props.turnloss_hg_intime_pulled;

    return (
      <div className={"graph-container"}>
        <Row type={"flex"} justify={"space-around"} align={"top"}>
          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Histogram </h1>
              <Graphs loading={!go}>
                <HCHistogram
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.histogram_hg[this.props.selectedModeIndex]
                  }
                  style={this.state.display ? {} : { display: "none" }}
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Histogram Intime </h1>
              <Graphs loading={!go}>
                <HCHistogramIntime
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.histogram_hg_intime[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Integral </h1>
              <Graphs loading={!go}>
                <HCIntegral
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.integral_hg[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Integral intime </h1>
              <Graphs loading={!go}>
                <HCIntegralIntime
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.integral_hg_intime[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Integral distribution </h1>
              <Graphs loading={!go}>
                <HCIntegralDist
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.integral_distrib_hg[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Raw distribution </h1>
              <Graphs loading={!go}>
                <HCRawDist
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.raw_distrib_hg[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Turnloss</h1>
              <Graphs loading={!go}>
                <HCTurnloss
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.turnloss_hg[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>

          <Col className={"graph-col"} xs={24} lg={11}>
            <div className={"detail"}>
              <h1> Turnloss intime </h1>
              <Graphs loading={!go}>
                <HCTurnlossIntime
                  devices={this.props.devices}
                  datasToPlot={
                    this.props.turnloss_hg_intime[this.props.selectedModeIndex]
                  }
                />
              </Graphs>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  actionCreators
)(GraphContainer);
