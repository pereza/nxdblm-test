import React, { Component } from "react";
import { connect } from "react-redux";
import FillData from "../components/FillData";
import * as actionCreators from "../Store/actions/rootActions";

class FillDataContainer extends Component {
  componentDidMount() {
    this.props.cleanStoreForName(this.props.match.params.name);
    this.props.setFillByName(this.props.match.params.name);
  }

  render() {
    const fillData =
      this.props.fill && this.props.modes[0] ? (
        <FillData
          history={this.props.history}
          fill={this.props.fill}
          modes={this.props.modes}
        />
      ) : (
        <div className="loading-fill-data" />
      );

    return <div>{fillData}</div>;
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  actionCreators
)(FillDataContainer);
